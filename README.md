# DroneDemoTask

This app was created by Ozougwu Ifeanyi Reginald ozougwifeanyi160@gmail.com

### Introduction

There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

---



## Development

This project is designed using Domain Driven Design (DDD) architectural monolitic design,
dependency injection and abstraction are highly conversed using contructor injection in most cases.



## Steps to Setup

**install java 17 or above**

## Build

The application can be built from the startUp class

## API Request Documentation
Swagger UI via http://localhost:16009/swagger-ui/index.html
It is advisable to browse through the swagger documentsion to understand more 
about the signature of the API.

## DataBase
In built database with seeded data

## Background task/Cron Job
There a background task that check the battery capacity of every drone for a specific period of time.

## Log
app.log file has all the build related events and information
error.log file has all the error and exception related logs.
events.log file has all the successful CRUD events 
droneAudit.log file has the logs for background jobs events.

## Controller/Resources
Drone Resources: performs CRUD operations for drone model
Medication Resources: performs CRUD operations for medication model
File Resources: upload file and return the file path as a Json response
Dispatch Resources: performs the main task which involves loading the drones with the
medcation items, getting the available drone and medication


## Test 
The endpoints can be tested using swagger or Junit testing which is currently present
in the test directory of the Application
## Steps for Drone Test Environment
1. run the first on getAllDrones method in side DoneTest class to obtain drone responses.
2. Pick the response data eg Id to enable you test the remaining endpoints

## Steps for Drone Medication Environment
1. run the first on getAllDrones method in side MedicationTest class to obtain drone responses.
2. Pick the response data eg Id to enable you test the remaining endpoints.

## Steps for Drone Dispatch Environment
1. Run your first and second test on getAvailabledrone and getAvaialbleMedications, then use the respose data on the LoadDrone endpoint.
2.Kindly use the response data from drone and medication test to complete the test on distpatchTest class

## Note
There is a Junit test cases for most of the endpoints excluding Create drone endpoint 
which is having a multipart/form-dat Content-Type.
Kindly test this endpoint using swagger or post.


