package com.drone_demo_task.dispatch;

import com.drone_demo_task.util.BaseController;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/api/dispatch", produces = MediaType.APPLICATION_JSON_VALUE)
public class DispatchResource extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(DispatchResource.class);
    private final DispatchServices dispatchServices;

    public DispatchResource(DispatchServices dispatchServices) {
        this.dispatchServices = dispatchServices;
    }

    @GetMapping("/availableMedications")
    public ResponseEntity<?> GetAvailableMedications() {
      return  Response(dispatchServices.GetAvailableMedications());
    }
    @GetMapping("/availableDrones")
    public ResponseEntity<?> GetAvailableDrones() {
        logger.info("This is an info message");

        return  Response(dispatchServices.GetAvailableDrone());
    }

    @PostMapping("/LoadDrone")
    public ResponseEntity<?> LoadDrone(@RequestBody @Valid LoadDroneDTO dto) {
        return  Response(dispatchServices.LoadDrone(dto));
    }
}
