package com.drone_demo_task.dispatch;

import com.drone_demo_task.drone.Drone;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class AvailableMedicationDTO {
    private Long id;

    @JsonIgnore
    private Drone drone;
    private String name;
    private Double Weight;
    private String Code;
    private String ImagePath;

}
