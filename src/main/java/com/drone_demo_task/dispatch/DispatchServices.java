package com.drone_demo_task.dispatch;

import com.drone_demo_task.drone.Drone;
import com.drone_demo_task.drone.DroneListDTO;
import com.drone_demo_task.drone.DroneRepository;
import com.drone_demo_task.drone.DroneState;
import com.drone_demo_task.medications.MedicationListDto;
import com.drone_demo_task.medications.Medications;
import com.drone_demo_task.medications.MedicationsRepository;
import com.drone_demo_task.util.ApiResponse;
import jakarta.persistence.Id;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class DispatchServices{

    private final MedicationsRepository medicationsRepository;
    private final DroneRepository droneRepository;
    @Autowired
    private ModelMapper modelMapper;

    public DispatchServices(MedicationsRepository medicationsRepository, DroneRepository droneRepository) {
        this.medicationsRepository = medicationsRepository;
        this.droneRepository = droneRepository;

    }

    public ApiResponse GetAvailableDrone()
    {
        final List<Drone> availableDrones = droneRepository.findAll(Sort.by("id"));

        List<AvailableDroneDTO> response = Arrays.asList(modelMapper.map(availableDrones, AvailableDroneDTO[].class))
                        .stream().filter(x-> x.getLoad() < x.getWeight() &&
                        x.getState() != DroneState.LOADED).collect(Collectors.toList());
        return new ApiResponse(
                HttpStatus.OK.value(),
                "success",
                response
        );

    }
    public ApiResponse GetAvailableMedications()
    {

        var  medications = medicationsRepository.findAll(Sort.by("id"));
        List<AvailableMedicationDTO> response = Arrays.asList(modelMapper.map(medications, AvailableMedicationDTO[].class))
                .stream().filter(med -> med.getDrone() == null).collect(Collectors.toList());
        return new ApiResponse(
                HttpStatus.OK.value(),
                "success",
                response
        );
    }
    public ApiResponse LoadDrone(LoadDroneDTO dto)
    {
        Optional<Drone> drone = droneRepository.findById(dto.getDroneId());

        if(!drone.isPresent())
        {
            return new ApiResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "drone with id: " + dto.getDroneId() + " does not exist",
                    null
            );
        }
        if(drone.get().getBatteryCapacity() < 25)
        {
            return new ApiResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "the drone battery capity is " + drone.get().getBatteryCapacity() +"" +
                            "% which is below 25% percent, this drone can not be loaded. Kindly recharge the battery",
                    null
            );
        }

        var  medications = medicationsRepository.findAll(Sort.by("id"));
        Double droneWeight = drone.get().getWeight();
        Double weightToBeLoaded = 0.0;
        Double currentLoad = drone.get().getLoad();

        for (long medicationId: dto.getMedicationsId())
        {
            var existingMedication = medicationsRepository.findById(medicationId);
           if(!existingMedication.isPresent())
           {
               return new ApiResponse(
                       HttpStatus.BAD_REQUEST.value(),
                       "meditation with id " +medicationId + " does not exist",
                       null
               );
           }
           else {
               if(existingMedication.get().getDrone_Id() != null)
               {
                   return new ApiResponse(
                           HttpStatus.BAD_REQUEST.value(),
                           "meditation with id " +medicationId + " has already been loaded to a drone",
                           null
                   );
               }
               weightToBeLoaded =+ existingMedication.get().getWeight();


           }
        }


        if(weightToBeLoaded > droneWeight)
        {
            return new ApiResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "Weight of the medications has exeeded the drone weight limit of "+ droneWeight + "grams",
                    null
            );
        }


        var totalWeight = currentLoad + weightToBeLoaded;
        if(totalWeight > droneWeight)
        {
            return new ApiResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "Weight of the medications has exeeded the drone weight limit of "+ droneWeight+ "grams",
                    null
            );

        }
        var loadingMedications = medicationsRepository.findAllById(dto.getMedicationsId());
        for (Medications medic: loadingMedications) {
            medic.setDrone_Id(drone.get().getId());
            medic.setDrone(drone.get());

        }
        try{
            if(totalWeight == droneWeight)
            {
                drone.get().setState(DroneState.LOADED);
            }
            else {
                drone.get().setState(DroneState.LOADING);
            }
            drone.get().setLoad(totalWeight);
            droneRepository.save(drone.get());
            medicationsRepository.saveAll(new ArrayList<Medications>(loadingMedications));
        }
        catch (Exception exp)
        {
            return new ApiResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    exp.getMessage(),
                    null
            );

        }

        return new ApiResponse(
                HttpStatus.OK.value(),
                "drone has been loaded succesfully",
                loadingMedications
        );

    }


}
