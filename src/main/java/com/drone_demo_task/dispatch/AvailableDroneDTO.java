package com.drone_demo_task.dispatch;

import com.drone_demo_task.drone.DroneListDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AvailableDroneDTO extends DroneListDTO {
    @JsonIgnore

    private Double Load;
}
