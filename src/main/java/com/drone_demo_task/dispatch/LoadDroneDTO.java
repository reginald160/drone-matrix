package com.drone_demo_task.dispatch;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data

public class LoadDroneDTO {

    @NotNull
    private Long DroneId;
    @NotNull
    private List<Long> MedicationsId;

    public LoadDroneDTO(Long droneId, List<Long> medicationsId) {
        DroneId = droneId;
        MedicationsId = medicationsId;
    }
}
