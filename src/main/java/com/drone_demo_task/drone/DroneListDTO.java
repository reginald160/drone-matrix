package com.drone_demo_task.drone;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;


@Getter
@Setter
public class DroneListDTO {

    private Long id;

    private String serialNumber;

    private DroneState state;

    private DroneModel model;

    private Double Weight;
    private Double BatteryCapacity;

    private OffsetDateTime dateCreated;
    private OffsetDateTime lastUpdated;
}
