package com.drone_demo_task.drone;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;


@Getter
@Setter
public class DroneDTO {


    @Min( value = 1,message = "weight must be greater than 1")
    @Max( value = 500,message = "weight must be not be greater 500")
    private Double Weight;
    @Min(value = 1, message = "battery capacity must be greater than 1")
    @Max( value = 100,message = "battery capacity must not exceed 100")
    private Double BatteryCapacity;
    @NotNull
    private DroneState state;

    @NotNull

    private MultipartFile file;
    @NotNull
    private DroneModel model;
    @JsonIgnore
    private String imagePath;

}
