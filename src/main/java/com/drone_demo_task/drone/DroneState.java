package com.drone_demo_task.drone;


public enum DroneState {

    IDLE, LOADING,
    LOADED, DELIVERING,
    DELIVERED, RETURNING

}
