package com.drone_demo_task.drone;

import com.drone_demo_task.util.ApiResponse;
import com.drone_demo_task.util.Logics;
import com.drone_demo_task.util.NotFoundException;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


@Service
public class DroneService {

    private final DroneRepository droneRepository;
    private static final Logger logger = LoggerFactory.getLogger("com.app.events");

    @Autowired
    private ModelMapper modelMapper;
    public DroneService(final DroneRepository droneRepository) {
        this.droneRepository = droneRepository;
    }

    public ApiResponse findAll() {
       try{
           final List<Drone> drones = droneRepository.findAll(Sort.by("id"));

           List<DroneListDTO> response = Arrays.asList(modelMapper.map(drones, DroneListDTO[].class));
           return new ApiResponse(
                   HttpStatus.OK.value(),
                   "success",
                   response
           );

       }
       catch (Exception exp)
       {
           return new ApiResponse(
                   HttpStatus.INTERNAL_SERVER_ERROR.value(),
                   exp.getMessage(),
                   null
           );
       }
    }

    public ApiResponse get(final Long id) {
        var drone =  droneRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        var record = modelMapper.map(drone, DroneListDTO.class);
        return new ApiResponse(
                HttpStatus.OK.value(),
                "record found",
                drone

        );

    }

    public ApiResponse create( DroneDTO droneDTO) {
        Drone drone = modelMapper.map(droneDTO, Drone.class);
        String serialNumber = Logics.GenerateSerialNumber();
        drone.setSerialNumber(serialNumber);
        drone.setLoad(0.0);
       var id = droneRepository.save(drone).getId();
        var message = "Drone has been created " +
                "Drone[SerialNumber=" + drone.getSerialNumber() + "," +
                " state=" + drone.getState() + ", model=" + drone.getModel() +
                ", weight=" + drone.getWeight() + "]";
        logger.info(message);
        return new ApiResponse(
                HttpStatus.OK.value(),
                "record has been created",
                droneRepository.findById(id)

        );
    }

    public ApiResponse update(final Long id, final DroneUpdateDTO droneDTO) {
        final Drone drone = droneRepository.findById(id)
                .orElseThrow(NotFoundException::new);
       var updatedDrone =  mapToEntityUpdate(droneDTO, drone);

        drone.setLoad(drone.getLoad());
        droneRepository.save(updatedDrone);
        var message = "Drone has been updated " +
                "Drone[SerialNumber=" + drone.getSerialNumber() + "," +
                " state=" + drone.getState() + ", model=" + drone.getModel() +
                ", weight=" + drone.getWeight() + "]";
        logger.info(message);
        return new ApiResponse(
                HttpStatus.OK.value(),
                "record has been updated",
                droneRepository.findById(id)

        );
    }

    public ApiResponse delete(final Long id) {

        var drone = droneRepository.findById(id).orElseThrow(NotFoundException::new);
        droneRepository.deleteById(id);
        return new ApiResponse(
                HttpStatus.OK.value(),
                "record has been deleted",
                null

        );
    }


    private Drone mapToEntity(final DroneDTO droneDTO, final Drone drone) {
        drone.setModel(droneDTO.getModel());
        //drone.setSerialNumber(droneDTO.getSerialNumber());
        drone.setWeight(droneDTO.getWeight());
        drone.setBatteryCapacity(droneDTO.getBatteryCapacity());
        drone.setState(droneDTO.getState());
        return drone;
    }
    private Drone mapToEntityUpdate(final DroneUpdateDTO droneDTO, final Drone drone) {
        drone.setModel(droneDTO.getModel());
        //drone.setSerialNumber(droneDTO.getSerialNumber());
        drone.setWeight(droneDTO.getWeight());
        drone.setBatteryCapacity(droneDTO.getBatteryCapacity());
        drone.setState(droneDTO.getState());
        if(!droneDTO.getImagePath().isEmpty() && droneDTO.getImagePath() != null ){
            drone.setImagePath(droneDTO.getImagePath());
        }
        return drone;
    }

    public boolean serialNumberExists(final String serialNumber) {
        return droneRepository.existsBySerialNumberIgnoreCase(serialNumber);
    }

}
