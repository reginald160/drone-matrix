package com.drone_demo_task.drone;

public enum DroneModel {
    Lightweight, Middleweight,
    Cruiserweight, Heavyweight
}
