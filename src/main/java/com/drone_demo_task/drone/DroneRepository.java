package com.drone_demo_task.drone;

import org.springframework.data.jpa.repository.JpaRepository;


public interface DroneRepository extends JpaRepository<Drone, Long> {

    boolean existsBySerialNumberIgnoreCase(String serialNumber);

}
