package com.drone_demo_task.drone;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import java.time.OffsetDateTime;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class Drone {

    @Id
    @Column(nullable = false, updatable = false)
    @SequenceGenerator(
            name = "primary_sequence",
            sequenceName = "primary_sequence",
            allocationSize = 1,
            initialValue = 10000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "primary_sequence"
    )
    private Long id;

    @Column(nullable = false, unique = true)
    private String serialNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DroneState state;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DroneModel model;

    private Double Weight;
    @JsonIgnore

    private Double Load;
    private Double BatteryCapacity;

    @CreatedDate
    @Column(nullable = false, updatable = false)
    private OffsetDateTime dateCreated;

    @LastModifiedDate
    @Column(nullable = false)
    private OffsetDateTime lastUpdated;
    private String imagePath;
    public Drone(){

    }


    public Drone(String serialNumber, DroneState state, DroneModel model, Double weight, Double batteryCapacity, String ImagePath) {
        this.serialNumber = serialNumber;
        this.state = state;
        this.model = model;
        this.Weight = weight;
        this. BatteryCapacity = batteryCapacity;
        this.setLoad(0.0);
        this.imagePath = ImagePath;
    }
    public Drone(String serialNumber, DroneState state, DroneModel model, Double weight, Double batteryCapacity) {
        this.serialNumber = serialNumber;
        this.state = state;
        this.model = model;
        this.Weight = weight;
        this. BatteryCapacity = batteryCapacity;
        this.setLoad(0.0);
    }
}
