package com.drone_demo_task.drone;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class DroneUpdateDTO {
    @Min( value = 1,message = "weight must be greater than 1")
    @Max( value = 500,message = "weight must be not be greater 500")
    private Double Weight;
    @Min(value = 1, message = "battery capacity must be greater than 1")
    @Max( value = 100,message = "battery capacity must not exceed 100")
    private Double BatteryCapacity;
    @NotNull
    private DroneState state;

    @NotNull
    private DroneModel model;
    private String imagePath;
}
