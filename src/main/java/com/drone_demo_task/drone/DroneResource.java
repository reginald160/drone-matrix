package com.drone_demo_task.drone;

import com.drone_demo_task.DroneDemoTaskApplication;
import com.drone_demo_task.util.BaseController;
import com.drone_demo_task.util.Logics;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.sound.midi.Patch;

import static org.springframework.web.bind.annotation.RequestMethod.*;


@RestController
@RequestMapping(value = "/api/drones", produces = MediaType.APPLICATION_JSON_VALUE)
public class DroneResource extends BaseController {

    private final DroneService droneService;
    private static final Logger logger = LoggerFactory.getLogger("com.app.errors");

    public DroneResource(final DroneService droneService) {
        this.droneService = droneService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getAllDrones() {
        return Response(droneService.findAll());
    }

    @GetMapping("/getById{id}")
    public ResponseEntity<?> getDrone(@PathVariable(name = "id") final Long id) {
        return Response(droneService.get(id));
    }

    @RequestMapping(path = "/register", method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    @ApiResponse(responseCode = "200")
    public  ResponseEntity<?>  register(@ModelAttribute  @Valid DroneDTO droneDTO  ) throws IOException {


        var fileName = droneDTO.getFile().getOriginalFilename();
        //check for image format
        if(!Logics.IsImageFormat(fileName))
        {
            var response =  new com.drone_demo_task.util.ApiResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "has invalid file format, kindly upload only image files",
                    fileName
            );
            return  Response(response);
        }
        //Image Upload
        var imagePath = Logics.FileUpload(droneDTO.getFile());
        droneDTO.setImagePath(imagePath);
        return  Response(droneService.create(droneDTO));

    }



    @PutMapping("/update{droneId}")
    public ResponseEntity<?> update(@PathVariable(name = "droneId") @Valid final Long droneId, @RequestBody @Valid final DroneUpdateDTO droneDTO)
    {
        return  Response(droneService.update(droneId,droneDTO));

    }
    @DeleteMapping("/delete{id}")
    @ApiResponse(responseCode = "200")
    public ResponseEntity<?> deleteDrone(@PathVariable(name = "id") final Long id) {

        return Response(droneService.delete(id));
    }

}
