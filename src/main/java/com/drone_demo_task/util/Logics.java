package com.drone_demo_task.util;

import com.drone_demo_task.DroneDemoTaskApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Logics {
    public static String GenerateSerialNumber ()
    {
        String str = String.valueOf(System.currentTimeMillis());
        String suffix = str.substring(Math.max(str.length() - 5, 0));
        return AppConstants.SERIAL_NUMBER_PREFIX + suffix;
    }

    public static  String FileUpload(MultipartFile file) throws IOException {


        // Get the path to the resources folder
        String resourcesDirPath = DroneDemoTaskApplication.class.getClassLoader().getResource("").getPath();
        String imgFolderPath = resourcesDirPath + "img/";
        File imgFolder = new File(imgFolderPath);

        // Save the uploaded file in the "img" folder
        var path = imgFolder.getAbsolutePath() + File.separator + file.getOriginalFilename();
        Path filePath = Paths.get(path);
        Files.write(filePath, file.getBytes());
        return path;
    }

    public static String getFileFormat(String fileName) {
        int lastDotIndex = fileName.lastIndexOf('.');
        if (lastDotIndex > 0 && lastDotIndex < fileName.length() - 1) {
            return fileName.substring(lastDotIndex + 1).toLowerCase();
        }
        return "";
    }
    public static Boolean IsImageFormat(String fileName) {
        String fileFormat = getFileFormat(fileName).toLowerCase();
        String [] imageFormats = {"jpg", "jpeg", "png", "gif", "bmp"};

        for (String format: imageFormats)
        {
            if( format.equals(fileFormat))
                return  true;

        }

        return false;
    }

}
