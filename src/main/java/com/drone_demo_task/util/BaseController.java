package com.drone_demo_task.util;

import org.springframework.http.ResponseEntity;

public abstract class BaseController {
    public ResponseEntity<?> Response(ApiResponse response)
    {

        ResponseEntity<?> result;

        switch (response.getStatusCode()) {
            case  200, 201:
                result = ResponseEntity.ok().body(response);
                break;
            case 400:
                result = ResponseEntity.badRequest().body(response);
                break;
            case 404:
                 throw new NotFoundException(response.getMessage());
            default:
                result = ResponseEntity.internalServerError().body(response);
                break;
        }
        return result;

    }
}
