package com.drone_demo_task.util;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/api/file", produces = MediaType.APPLICATION_JSON_VALUE)

public class FileResources extends  BaseController{
    @RequestMapping( path = "/uploadFile", method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<?> uploadFile(@RequestBody @Valid final MultipartFile  file) throws IOException {

        var fileName = file.getOriginalFilename();
        //check for image format
        if(!Logics.IsImageFormat(fileName))
        {
            var response =  new com.drone_demo_task.util.ApiResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "has invalid file format, kindly upload only image files",
                    fileName
            );
            return  Response(response);
        }
        //Image Upload
        var imagePath = Logics.FileUpload(file);
        var response =  new com.drone_demo_task.util.ApiResponse(
                HttpStatus.OK.value(),
                "file has been uploaded",
                imagePath
        );
        return  Response(response);


   }
}
