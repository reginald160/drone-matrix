package com.drone_demo_task.util;

public class AppConstants {
    public static final String SERIAL_NUMBER_PREFIX = "SDSD";
    public static final String MEDICATION_CODE_REGEX  = "^[A-Z0-9_]*$";
    public static final String MEDICATION_NAME_REGEX = "^[-a-zA-Z0-9-_]+$";
    public  static  final  String CRON_EXPRESSION = "0 */5 * ? * *";
    public  static final  String DEFAULT_IMAGE_PATH = "C:\\Users\\Ozougwudr\\source\\JavaDemo\\drone-demo-task\\target\\classes\\img\\6.jpg";
}
