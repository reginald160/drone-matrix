package com.drone_demo_task.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

@Data
@Getter
@Setter
@JsonPropertyOrder({"StatusCode","Message", "Data"})
@AllArgsConstructor
@NoArgsConstructor

public class ApiResponse {
    @JsonProperty("StatusCode")
    private Integer StatusCode;
    @JsonProperty("Message")
    private String   Message;
    @JsonProperty("Data")
    private Object Data;


}
