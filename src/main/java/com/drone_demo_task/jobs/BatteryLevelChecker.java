package com.drone_demo_task.jobs;


import com.drone_demo_task.dispatch.DispatchResource;
import com.drone_demo_task.drone.Drone;
import com.drone_demo_task.drone.DroneRepository;
import com.drone_demo_task.drone.DroneState;
import com.drone_demo_task.util.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class BatteryLevelChecker {

    private final DroneRepository droneRepository;
    private static final Logger logger = LoggerFactory.getLogger("com.app.droneAudit");


    public BatteryLevelChecker(DroneRepository droneRepository) {
        this.droneRepository = droneRepository;
    }

    @Scheduled(cron = AppConstants.CRON_EXPRESSION)
    public void checkDatabaseTable() {
        var drones = droneRepository.findAll();
        for (Drone drone: drones)
        {
            if(drone.getBatteryCapacity() < 25)
            {
                drone.setState(DroneState.IDLE);
                droneRepository.save(drone);
            }
            var message = "BATTERY CHECKER:::::::::::" +
                    "  done { serial number : " + drone.getSerialNumber() + " battery capacity : "+ drone.getBatteryCapacity() +"% }";
            logger.info(message);
        }

    }
}

