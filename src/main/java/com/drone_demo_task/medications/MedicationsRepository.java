package com.drone_demo_task.medications;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface MedicationsRepository extends JpaRepository<Medications, Long> {

    boolean existsByNameIgnoreCase(String name);
    List<Medications> findByDroneIsNull();


}
