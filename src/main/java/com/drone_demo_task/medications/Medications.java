package com.drone_demo_task.medications;

import com.drone_demo_task.drone.Drone;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import java.time.OffsetDateTime;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import static jakarta.persistence.CascadeType.ALL;


@Entity
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class Medications {

    @Id
    @Column(nullable = false, updatable = false)
    @SequenceGenerator(
            name = "primary_sequence",
            sequenceName = "primary_sequence",
            allocationSize = 1,
            initialValue = 10000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "primary_sequence"
    )
    private Long id;


    @Column(nullable = false, unique = true)
    private String name;
//    @Column(nullable = false, unique = true)
//    private Long drone_id;

    @OneToOne(fetch = FetchType.LAZY, cascade = ALL)
    @JoinColumn(name = "drone_FK", nullable = true)
    private Drone drone;
    @JsonIgnore
    private  Long drone_Id;

    private Double Weight;

    private String Code;
    private String ImagePath;

    @CreatedDate
    @Column(nullable = false, updatable = false)
    private OffsetDateTime dateCreated;

    @LastModifiedDate
    @Column(nullable = false)
    private OffsetDateTime lastUpdated;
    public Medications() {

    }
    public Medications(String name,
                       Double weight, String code,
                       String imagePath)
    {

        this.name = name;
        this.Weight = weight;
        this.Code = code;
        this.ImagePath = imagePath;
    }
}
