package com.drone_demo_task.medications;

import com.drone_demo_task.util.BaseController;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/medications", produces = MediaType.APPLICATION_JSON_VALUE)
public class MedicationsResource extends BaseController {

    private final MedicationsService medicationsService;

    public MedicationsResource(final MedicationsService medicationsService) {
        this.medicationsService = medicationsService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getAllMedications() {
        return Response(medicationsService.findAll());
    }

    @GetMapping("/getById{id}")
    public ResponseEntity<?> getMedicationById(@PathVariable(name = "id") final Long id) {
        return Response(medicationsService.get(id));
    }

    @PostMapping("/registration")
    @ApiResponse(responseCode = "200")
    public ResponseEntity<?> createMedications(
            @RequestBody @Valid final MedicationsDTO medicationsDTO) {
        return  Response(medicationsService.create(medicationsDTO));
    }

    @PutMapping("/update{id}")
    public ResponseEntity<?> updateMedications(@PathVariable(name = "id") final Long id,
            @RequestBody @Valid final MedicationsDTO medicationsDTO) {
           return Response(medicationsService.update(id, medicationsDTO));


    }

    @DeleteMapping("/delete{id}")
    @ApiResponse(responseCode = "200")
    public ResponseEntity<?> deleteMedications(@PathVariable(name = "id") final Long id) {

        return Response(medicationsService.delete(id));
    }

}
