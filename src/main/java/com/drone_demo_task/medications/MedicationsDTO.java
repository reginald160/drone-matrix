package com.drone_demo_task.medications;

import com.drone_demo_task.util.AppConstants;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MedicationsDTO {

    @NotBlank
    @Size(max = 255)
    @Pattern(regexp = AppConstants.MEDICATION_NAME_REGEX, message = "only only letters, numbers, ‘-‘, ‘_’ are allowed")
    private String name;
    @Min(value = 1, message = "value can not be more be less than 1")
    @Max(value = 400, message = "value can not be more be more than 400")
    private Double Weight;

    @NotBlank
    @Pattern(regexp = AppConstants.MEDICATION_CODE_REGEX, message = "only upper case letters, underscore and numbers")
    private String Code;


}
