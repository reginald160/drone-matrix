package com.drone_demo_task.medications;

import com.drone_demo_task.drone.Drone;
import com.drone_demo_task.drone.DroneListDTO;
import com.drone_demo_task.drone.DroneRepository;
import com.drone_demo_task.util.ApiResponse;
import com.drone_demo_task.util.NotFoundException;

import java.util.Arrays;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


@Service
public class MedicationsService {

    private final MedicationsRepository medicationsRepository;
    private final DroneRepository droneRepository;
    @Autowired
    private ModelMapper modelMapper;

    public MedicationsService(final MedicationsRepository medicationsRepository,
            final DroneRepository droneRepository) {
        this.medicationsRepository = medicationsRepository;
        this.droneRepository = droneRepository;
    }

    public ApiResponse findAll() {
        final List<Medications> medications = medicationsRepository.findAll(Sort.by("id"));
        List<MedicationListDto> response = Arrays.asList(modelMapper.map(medications, MedicationListDto[].class));
        return new ApiResponse(
                HttpStatus.OK.value(),
                "success",
                response
        );

    }

    public ApiResponse get(final Long id) {
        var result = medicationsRepository.findById(id).orElseThrow(NotFoundException::new);
        var medication = modelMapper.map(result,MedicationListDto.class);
        return new ApiResponse(
                HttpStatus.OK.value(),
                "success",
                medication
        );
    }

    public ApiResponse create(final MedicationsDTO medicationsDTO) {

        if(nameExists(medicationsDTO.getName()))
        {
            return new ApiResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "record with name " + medicationsDTO.getName()+ " already exists",
                    medicationsDTO

            );
        }

        final Medications medications = modelMapper.map(medicationsDTO,Medications.class);
        var id =  medicationsRepository.save(medications).getId();
        return new ApiResponse(
                HttpStatus.OK.value(),
                "record has been created",
                id

        );
    }

    public ApiResponse update(final Long id, final MedicationsDTO medicationsDTO) {
        final Medications medications = medicationsRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        mapToEntity(medicationsDTO, medications);
        medicationsRepository.save(medications);
        return new ApiResponse(
                HttpStatus.OK.value(),
                "record has been updated",
                id

        );
    }

    public ApiResponse delete(final Long id) {
        medicationsRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        medicationsRepository.deleteById(id);
        return new ApiResponse(
                HttpStatus.OK.value(),
                "record has been deleted",
                id

        );
    }

    private MedicationsDTO mapToDTO(final Medications medications,
            final MedicationsDTO medicationsDTO) {
//        medicationsDTO.setId(medications.getId());
        medicationsDTO.setName(medications.getName());
        //medicationsDTO.setDroneId(medications.getDrone() == null ? null : medications.getDrone().getId());
        return medicationsDTO;
    }

    private Medications mapToEntity(final MedicationsDTO medicationsDTO,
            final Medications medications) {
        medications.setName(medicationsDTO.getName());

        return medications;
    }

    public boolean nameExists(final String name) {
        return medicationsRepository.existsByNameIgnoreCase(name);
    }


}
