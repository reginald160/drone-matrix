package com.drone_demo_task.medications;

import com.drone_demo_task.drone.Drone;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;

@Getter
@Setter
public class MedicationListDto {

    private Long id;

    private String name;

    private Long droneId;
    private Double Weight;
    private String Code;
    private String ImagePath;

    private OffsetDateTime dateCreated;

    private OffsetDateTime lastUpdated;


}
