package com.drone_demo_task.config;

import com.drone_demo_task.drone.Drone;
import com.drone_demo_task.drone.DroneModel;
import com.drone_demo_task.drone.DroneRepository;
import com.drone_demo_task.drone.DroneState;
import com.drone_demo_task.medications.Medications;
import com.drone_demo_task.medications.MedicationsRepository;
import com.drone_demo_task.util.AppConstants;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;


@Configuration
public class DroneConfig {
    @Bean
    CommandLineRunner DroneCommandLineRunner (DroneRepository repository)
    {
        return  arg -> {
            Drone Mavic  = new Drone("SDSD101212", DroneState.IDLE, DroneModel.Heavyweight, 500.0, 100.0, AppConstants.DEFAULT_IMAGE_PATH);
            Drone Parrot  = new Drone("SDSD10332", DroneState.IDLE, DroneModel.Lightweight, 200.0, 100.0,AppConstants.DEFAULT_IMAGE_PATH);
            Drone Skydio  = new Drone("SDSD10442", DroneState.IDLE, DroneModel.Cruiserweight, 450.0, 80.0,AppConstants.DEFAULT_IMAGE_PATH);
            Drone Yuneec  = new Drone("SDSD10329", DroneState.IDLE, DroneModel.Middleweight, 300.0, 25.0,AppConstants.DEFAULT_IMAGE_PATH);
            Drone Baka  = new Drone("SDSD103252", DroneState.IDLE, DroneModel.Middleweight, 300.0, 20.0, AppConstants.DEFAULT_IMAGE_PATH);
            repository.saveAll(List.of(Mavic,Parrot,Skydio,Yuneec,Baka));
        };
    }
}
