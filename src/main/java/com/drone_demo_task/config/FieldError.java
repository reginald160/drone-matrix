package com.drone_demo_task.config;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class FieldError {

    private String field;
    private String errorCode;
    private String errorMessage;

}
