package com.drone_demo_task.config;

import com.drone_demo_task.medications.Medications;
import com.drone_demo_task.medications.MedicationsRepository;
import com.drone_demo_task.util.AppConstants;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;

@Configuration
public class MedicationConfig {
    @Bean
    CommandLineRunner commandLineRunner (MedicationsRepository repository)
    {
        return  arg -> {
            Medications Alfa = new Medications("Alfa", 100.0, "ALFA001", AppConstants.DEFAULT_IMAGE_PATH);
            Medications Pluto = new Medications("Pluto", 20.0, "PLUTO01",AppConstants.DEFAULT_IMAGE_PATH);
            Medications Beta = new Medications("Beta", 202.0, "BETA001",AppConstants.DEFAULT_IMAGE_PATH);
            Medications Ifeanyi = new Medications("Ifeanyi", 400.0, "IFEANYI122",AppConstants.DEFAULT_IMAGE_PATH);
            Medications Chigozie = new Medications("Chigozie", 400.0, "CHIGOZIE211",AppConstants.DEFAULT_IMAGE_PATH);
            Medications Yankee = new Medications("Yanke", 350.0, "YANKEE32",AppConstants.DEFAULT_IMAGE_PATH);
            Medications Umayi = new Medications("Umayi", 100.0, "UMAYI001",AppConstants.DEFAULT_IMAGE_PATH);
            Medications Mighty = new Medications("Mighty", 300.0, "MiGHTY102",AppConstants.DEFAULT_IMAGE_PATH);
            Medications Master = new Medications("Master", 500.0, "MASTER3635",AppConstants.DEFAULT_IMAGE_PATH);
            Medications Kany = new Medications("Kany", 20.0, "KANY1232",AppConstants.DEFAULT_IMAGE_PATH);

            repository.saveAll(List.of(
                    Alfa,Pluto,Beta,Ifeanyi,Chigozie,
                    Yankee,Umayi,Mighty,Master,Kany
            ));
        };
    }
}
