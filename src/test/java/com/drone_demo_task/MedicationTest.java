package com.drone_demo_task;



import com.drone_demo_task.medications.MedicationsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MedicationTest {

    private final String BaseUrl = "/api/medications/";
    @Autowired
    private MockMvc mockMvc;

    @org.junit.jupiter.api.Test
    public void getAllMedications() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(BaseUrl + "getAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("success"))
                .andDo(MockMvcResultHandlers.print());

    }

    @org.junit.jupiter.api.Test
    public void getDroneMedicationId() throws Exception {
        var endpoint = "getById" + "10005";
        mockMvc.perform(MockMvcRequestBuilders.get(BaseUrl + endpoint)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("success"))
                .andDo(MockMvcResultHandlers.print());

    }
    @org.junit.jupiter.api.Test
    public void deleteMedicationById() throws Exception {
        var endpoint = "delete" + "10005";
        mockMvc.perform(MockMvcRequestBuilders.delete(BaseUrl + endpoint)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("record has been deleted"))
                .andDo(MockMvcResultHandlers.print());

    }
    @Test
    public void UpdateDroneById() throws Exception {

        var endpoint = "update" + "10005";
        var requestBody =  "{\"name\":\"Ozougwu160-_\",\"code\":\"A11_\",\"weight\":1}";

        mockMvc.perform(MockMvcRequestBuilders
                        .put(BaseUrl + endpoint)
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("record has been updated"))
                .andReturn();


    }

    @Test
    public void CreateMedications() throws Exception {

        var endpoint = "registration";
        var requestBody =  "{\"name\":\"Ozougwuaa160-_\",\"code\":\"A11_\",\"weight\":12}";

        mockMvc.perform(MockMvcRequestBuilders
                        .post(BaseUrl + endpoint)
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("record has been created"))
                .andReturn();


    }

}

