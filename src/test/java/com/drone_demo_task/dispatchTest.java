package com.drone_demo_task;
import com.drone_demo_task.dispatch.LoadDroneDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class dispatchTest {

    private final String BaseUrl = "/api/dispatch/";
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllAvailableDrones() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(BaseUrl + "availableDrones")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("success"))
                .andDo(MockMvcResultHandlers.print());

    }
    @Test
    public void getAllAvailableMedication() throws Exception {


        mockMvc.perform(MockMvcRequestBuilders.get( BaseUrl +"availableMedications")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("success"))
                .andDo(MockMvcResultHandlers.print());
    }
    @Test
   public void LoadDrone() throws Exception {

        // kindly get the droneId and medicationId from the response on getAllMedication
    String requestBody = "{\"droneId\":10000, \"medicationsId\": [10007,10005]}";

                        mockMvc.perform(MockMvcRequestBuilders
                        .post(BaseUrl + "LoadDrone")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("drone has been loaded succesfully"))
                .andReturn();


    }
}





