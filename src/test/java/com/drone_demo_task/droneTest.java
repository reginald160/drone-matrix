package com.drone_demo_task;

import com.drone_demo_task.drone.DroneResource;
import com.drone_demo_task.drone.DroneService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class droneTest {

    private final String BaseUrl = "/api/drones/";
    @Autowired
    private MockMvc mockMvc;

    @org.junit.jupiter.api.Test
    public void getAllDrones() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(BaseUrl + "getAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("success"))
                .andDo(MockMvcResultHandlers.print());

    }
    @org.junit.jupiter.api.Test
    public void getDroneById() throws Exception {
        var endpoint = "getById" + "10000";
        mockMvc.perform(MockMvcRequestBuilders.get(BaseUrl + endpoint)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("record found"))
                .andDo(MockMvcResultHandlers.print());

    }

    @org.junit.jupiter.api.Test
    public void deleteDroneById() throws Exception {
        var endpoint = "delete" + "10000";
        mockMvc.perform(MockMvcRequestBuilders.delete(BaseUrl + endpoint)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("record has been deleted"))
                .andDo(MockMvcResultHandlers.print());

    }
    @Test
    public void UpdateDroneById() throws Exception {

        var endpoint = "update" + "10000";
       var requestBody =  "{\"state\":\"IDLE\",\"model\":\"Lightweight\"," +
               "\"imagePath\":\"string\",\"weight\":1,\"batteryCapacity\":1}";

        mockMvc.perform(MockMvcRequestBuilders
                        .put(BaseUrl + endpoint)
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("record has been updated"))
                .andReturn();


    }






}
